namespace DiscordTimestamps 
{
    public enum TimestampType : sbyte
    {
        ShortTime = (sbyte)'t',
        LongTime = (sbyte)'T',
        ShortDate = (sbyte)'d',
        LongDate = (sbyte)'D',
        ShortDateTime = (sbyte)'f',
        LongDateTime = (sbyte)'F',
        Relative = (sbyte)'R'
    }
    
    public static class Extensions
    {
        internal static char GetTimestampFlagInternal(this TimestampType type) => (char)type;
        
        internal static string GetDiscordTimestampInternal(long unixSeconds, char timestampType)
            => $"<t:{unixSeconds}:{timestampType}>";
        
        public static string GetDiscordTimestamp(this DateTimeOffset dto, TimestampType type) =>
            GetDiscordTimestampInternal(dto.ToUnixTimeSeconds(), type.GetTimestampFlagInternal());

        public static string GetDiscordTimestamp(this DateTime date, TimestampType type) => 
            new DateTimeOffset(date).GetDiscordTimestamp(type);
    }
    
}

// <DateTime/DateTimeOffset object>.GetDiscordTimestamp(TimestampType)